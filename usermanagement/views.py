from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.shortcuts import render, redirect

from .forms import SignUpForm


def signupView(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            signup_user = User.objects.get(username=username)
    else:
        form = SignUpForm()
    return render(request, 'account.html', {'form': form})


def signinView(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('')
        else:
            return redirect('signin')
    else:
        form = AuthenticationForm()
    return render(request, 'account.html', {'form': form})


def signoutView(request):
    logout(request)
    return redirect('signin')
