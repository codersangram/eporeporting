from django.db import models


class epo_db(models.Model):
    epo_id = models.IntegerField()
    EPO_DNS_Name = models.CharField(max_length=250, unique=False)
    IP_Address = models.CharField(max_length=100, unique=True)
    Date_Added = models.DateField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
