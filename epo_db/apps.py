from django.apps import AppConfig


class EpoDbConfig(AppConfig):
    name = 'epo_db'
